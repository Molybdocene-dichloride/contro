\selectlanguage *{russian}
\contentsline {section}{\numberline {1}Задание}{4}{}%
\contentsline {section}{\numberline {2}Монохлорирование изобутана}{5}{}%
\contentsline {subsection}{\numberline {2.1}Общий вид реакции}{5}{}%
\contentsline {subsection}{\numberline {2.2}Механизм реакции}{5}{}%
\contentsline {section}{\numberline {3}Бромирование циклопентена}{7}{}%
\contentsline {subsection}{\numberline {3.1}Общий вид реакции}{7}{}%
\contentsline {subsection}{\numberline {3.2}Механизм реакции}{7}{}%
\contentsline {section}{\numberline {4}Гидробромирование гексадиена-1,3}{9}{}%
\contentsline {subsection}{\numberline {4.1}Общий вид реакции}{9}{}%
\contentsline {subsection}{\numberline {4.2}Механизм реакции}{9}{}%
\contentsline {section}{\numberline {5}Мононитрование метилбензола}{12}{}%
\contentsline {subsection}{\numberline {5.1}Общий вид реакции}{12}{}%
\contentsline {subsection}{\numberline {5.2}Механизм реакции}{12}{}%
\contentsfinish 
